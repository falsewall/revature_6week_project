INSERT INTO customers (cust_name, points, phone, address) VALUES('Sol', 1001, '1234567890', 'Metro area');
INSERT INTO customers (cust_name, points, phone, address) VALUES('ManLey', 1000, '1111111111', 'Metro Area');
INSERT INTO customers (cust_name, points, phone, address) VALUES('Lenny', 12, '2222222222', 'Mako Rd.');
INSERT INTO customers (cust_name, points, phone, address) VALUES('Bobby', 12, '3333333333', 'Atlanta');
INSERT INTO customers (cust_name, points, phone, address) VALUES('Steve', 88, '4444444444', "Fairfax");
INSERT INTO customers (cust_name, points, phone, address) VALUES('Alice', 51, '5555555555',"Fauquier");
INSERT INTO customers (cust_name, points, phone, address) VALUES('Jerry', 104, '6666666666', "Fauquier");
INSERT INTO customers (cust_name, points, phone, address) VALUES('Bill', 204, '7777777777',"Fauquier");
INSERT INTO customers (cust_name, points, phone, address) VALUES('Robert', 34, '8888888888', "Louisa");
INSERT INTO customers (cust_name, points, phone, address) VALUES('Jennifer', 500, '9999999999', "Powhatan");
INSERT INTO customers (cust_name, points, phone, address) VALUES('Guy', 831, '0000000000', "Prince Edward");
INSERT INTO customers (cust_name, points, phone, address) VALUES('Person', 381, '0987654321', "Westmoreland");
INSERT INTO customers (cust_name, points, phone, address) VALUES('Calvin', 20, '2134567890',"Westmoreland");
INSERT INTO customers (cust_name, points, phone, address) VALUES('Dave', 101, '3214567890', "Tazewell");

INSERT INTO employees(employee_name, position, phone, hired, fired, sex, pto, address) VALUES( 'Lenny', 'Manager', '8045559999', '1999-10-12', null, 'm', 200, '2600 malchite dr.');
INSERT INTO employees(employee_name, position, phone, hired, fired, sex, pto, address) VALUES( 'Bobby', 'Cashier', '3333333333', '1992-08-18', null, 'm', 0, '2800 dirt dr.');
INSERT INTO employees(employee_name, position, phone, hired, fired, sex, pto, address) VALUES( 'Frank', 'Cashier', '7035559989', '1992-08-18', null, 'm', 75, '101 road rd.');
INSERT INTO employees(employee_name, position, phone, hired, fired, sex, pto, address) VALUES( 'Mark', 'Cashier', '7035559989', '1992-08-18', null, 'm', 33, '102 road rd.');
INSERT INTO employees(employee_name, position, phone, hired, fired, sex, pto, address) VALUES( 'Clark', 'Cashier', '7035559989', '1992-08-18', null, 'm', 42, '123 example dr.');
INSERT INTO employees(employee_name, position, phone, hired, fired, sex, pto, address) VALUES( 'Stark', 'Cashier', '7035559989', '1992-08-18', null, 'm', 24, '000 nowhere st.');
INSERT INTO employees(employee_name, position, phone, hired, fired, sex, pto, address) VALUES( 'Dave', 'Cashier', '3214567890', '1992-08-18', null, 'm', 9, '321 dirt dr.');
INSERT INTO employees(employee_name, position, phone, hired, fired, sex, pto, address) VALUES( 'Marvin', 'Cashier', '2314567890', '1992-08-18', null, 'm', 9, '321 dirt dr.');
INSERT INTO employees(employee_name, position, phone, hired, fired, sex, pto, address) VALUES( 'Nathan', 'Cashier', '3214567890', '1992-08-18', null, 'm', 9, '321 dirt dr.');


INSERT INTO inventory( item_name, stock_quantity, item_price, category) VALUES ( 'bread', 99, 3.50, 'baked');
INSERT INTO inventory( item_name, stock_quantity, item_price, category) VALUES ( 'scones', 12, 2.99, 'baked');
INSERT INTO inventory( item_name, stock_quantity, item_price, category) VALUES ( 'canned beans', 1, 3.50, 'canned');
INSERT INTO inventory( item_name, stock_quantity, item_price, category) VALUES ( 'canned bread', 20, 1.50, 'canned');
INSERT INTO inventory( item_name, stock_quantity, item_price, category) VALUES ( 'salt & vinegar chips', 15, 2.50, 'chips');
INSERT INTO inventory( item_name, stock_quantity, item_price, category) VALUES ( 'banana', 33, 3.10, 'fruit');
INSERT INTO inventory( item_name, stock_quantity, item_price, category) VALUES ( 'apple', 42, 3.20, 'fruit');
INSERT INTO inventory( item_name, stock_quantity, item_price, category) VALUES ( 'orange', 55, 3.30, 'fruit');
INSERT INTO inventory( item_name, stock_quantity, item_price, category) VALUES ( 'grape', 67, 3.40, 'fruit');
INSERT INTO inventory( item_name, stock_quantity, item_price, category) VALUES ( 'broccoli', 76, 3.50, 'vegetable');
INSERT INTO inventory( item_name, stock_quantity, item_price, category) VALUES ( 'celery', 88, 1.50, 'vegetable');
INSERT INTO inventory( item_name, stock_quantity, item_price, category) VALUES ( 'lettuce', 89, 2.50, 'vegetable');
INSERT INTO inventory( item_name, stock_quantity, item_price, category) VALUES ( 'carrots', 98, 1.50, 'vegetable');
INSERT INTO inventory( item_name, stock_quantity, item_price, category) VALUES ( 'cocacola', 99, 1.51, 'carbonated beverages');
INSERT INTO inventory( item_name, stock_quantity, item_price, category) VALUES ( 'pepsicola', 123, 2.52, 'carbonated beverages');
INSERT INTO inventory( item_name, stock_quantity, item_price, category) VALUES ( 'diet coke', 213, 4.50, 'carbonated beverages');
INSERT INTO inventory( item_name, stock_quantity, item_price, category) VALUES ( 'diet pepsi', 32, 3.55, 'carbonated beverages');
INSERT INTO inventory( item_name, stock_quantity, item_price, category) VALUES ( 'mountain dew', 111, 1.50, 'carbonated beverages');
INSERT INTO inventory( item_name, stock_quantity, item_price, category) VALUES ( 'sprite', 86, 1.10, 'carbonated beverages');

INSERT INTO transactions(employee_id, cust_id, trans_time,  trans_type)  VALUES(2, 1, current_timestamp(),  'Debit');#5 scones bought by ManLey, checked by Bobby
INSERT INTO transactions(employee_id, cust_id, trans_time,  trans_type)  VALUES(1, 2, current_timestamp(),  'Credit');# 1 can of beans bought by Lenny, checked out by Lenny
INSERT INTO transactions(employee_id, cust_id, trans_time,  trans_type)  VALUES(4, 3, current_timestamp(),  'Credit');
INSERT INTO transactions(employee_id, cust_id, trans_time,  trans_type)  VALUES(3, 4, current_timestamp(),  'Credit');
INSERT INTO transactions(employee_id, cust_id, trans_time,  trans_type)  VALUES(6, 5, current_timestamp(),  'Debit');
INSERT INTO transactions(employee_id, cust_id, trans_time,  trans_type)  VALUES(7, 6,current_timestamp(),  'Credit');
INSERT INTO transactions(employee_id, cust_id, trans_time,  trans_type)  VALUES(5, 7, current_timestamp(),  'Credit');
INSERT INTO transactions(employee_id, cust_id, trans_time,  trans_type)  VALUES(3, 8,current_timestamp(),  'Debit');
INSERT INTO transactions(employee_id, cust_id, trans_time,  trans_type)  VALUES(2, 9,current_timestamp(),  'Credit');
INSERT INTO transactions(employee_id, cust_id, trans_time,  trans_type)  VALUES(4, 10,current_timestamp(),  'Credit');
INSERT INTO transactions(employee_id, cust_id, trans_time,  trans_type)  VALUES(2, 1,current_timestamp(),  'Debit');
INSERT INTO transactions(employee_id, cust_id, trans_time,  trans_type)  VALUES(1, 2, current_timestamp(),  'Credit');
INSERT INTO transactions(employee_id, cust_id, trans_time,  trans_type)  VALUES(3, 3,current_timestamp(),  'Credit');
INSERT INTO transactions(employee_id, cust_id, trans_time, trans_type)  VALUES(5, 4, current_timestamp(), 'Credit');
INSERT INTO transactions(employee_id, cust_id, trans_time,  trans_type)  VALUES(2, 5, current_timestamp(), 'Debit');
INSERT INTO transactions(employee_id, cust_id, trans_time,  trans_type)  VALUES(4, 6, current_timestamp(),  'Credit');
INSERT INTO transactions(employee_id, cust_id, trans_time,  trans_type)  VALUES(3, 7, current_timestamp(),  'Credit');
INSERT INTO transactions(employee_id, cust_id, trans_time,  trans_type)  VALUES(4, 2, current_timestamp(),  'Debit');
INSERT INTO transactions(employee_id, cust_id, trans_time,  trans_type)  VALUES(7, 3, current_timestamp(),  'Credit');
INSERT INTO transactions(employee_id, cust_id, trans_time,  trans_type)  VALUES(4, 4, current_timestamp(),  'Credit');
INSERT INTO transactions(employee_id, cust_id, trans_time,  trans_type)  VALUES(9, 1, current_timestamp(),  'Credit');
INSERT INTO transactions(employee_id, cust_id, trans_time,  trans_type)  VALUES(8, 1, current_timestamp(),  'Credit');

INSERT INTO sales(trans_id,item_id,quantity) VALUES(2, 3, 1);
INSERT INTO sales(trans_id,item_id,quantity) VALUES(2, 5, 1);
INSERT INTO sales(trans_id,item_id,quantity) VALUES(3, 13, 1);
INSERT INTO sales(trans_id,item_id,quantity)  VALUES(4, 4, 2);
INSERT INTO sales(trans_id,item_id,quantity)  VALUES(5, 13, 2);
INSERT INTO sales(trans_id,item_id,quantity)  VALUES(6, 1, 3);
INSERT INTO sales(trans_id,item_id,quantity)  VALUES(1, 1, 4);
INSERT INTO sales(trans_id,item_id,quantity)  VALUES(4, 10, 4);
INSERT INTO sales(trans_id,item_id,quantity)  VALUES(3, 4, 4);
INSERT INTO sales(trans_id,item_id,quantity)  VALUES(1, 3, 5);
INSERT INTO sales(trans_id,item_id,quantity)  VALUES(1, 3, 6);
INSERT INTO sales(trans_id,item_id,quantity)  VALUES(9, 9, 8);
INSERT INTO sales(trans_id,item_id,quantity)  VALUES(10, 13, 8);
INSERT INTO sales(trans_id,item_id,quantity)  VALUES(4, 4, 9);
INSERT INTO sales(trans_id,item_id,quantity)  VALUES(13, 10, 9);
INSERT INTO sales(trans_id,item_id,quantity)  VALUES(13, 12, 10);
INSERT INTO sales(trans_id,item_id,quantity)  VALUES(14, 18, 11);
INSERT INTO sales(trans_id,item_id,quantity)  VALUES(5, 12, 12);
INSERT INTO sales(trans_id,item_id,quantity)  VALUES(2, 1, 13);
INSERT INTO sales(trans_id,item_id,quantity)  VALUES(5, 15, 14);
INSERT INTO sales(trans_id,item_id,quantity)  VALUES(9, 13, 15);
INSERT INTO sales(trans_id,item_id,quantity)  VALUES(9, 15, 16);
INSERT INTO sales(trans_id,item_id,quantity)  VALUES(6, 13, 16);
INSERT INTO sales(trans_id,item_id,quantity)  VALUES(8, 7, 17);
INSERT INTO sales(trans_id,item_id,quantity)  VALUES(13, 4, 17);
INSERT INTO sales(trans_id,item_id,quantity)  VALUES(14, 7, 18);
INSERT INTO sales(trans_id,item_id,quantity)  VALUES(11, 9, 19);
INSERT INTO sales(trans_id,item_id,quantity)  VALUES(5, 10, 20);
INSERT INTO sales(trans_id,item_id,quantity)  VALUES(9, 1, 20);
INSERT INTO sales(trans_id,item_id,quantity)  VALUES(2, 1, 7);
INSERT INTO sales(trans_id,item_id,quantity)  VALUES(11, 2, 2);
INSERT INTO sales(trans_id,item_id,quantity)  VALUES(1, 3, 2);

INSERT INTO timecards(employee_id,monday,tuesday,wednesday,thursday,friday,saturday,sunday,pto_used,date_friday)
 VALUES(2, 4.5,8,8,8,8,0,0,3.5, '1992-08-18');
INSERT INTO timecards(employee_id,monday,tuesday,wednesday,thursday,friday,saturday,sunday,pto_used,date_friday)
 VALUES(2, 8,8,8,8,8,0,0, 0, '1992-08-18');
INSERT INTO timecards(employee_id,monday,tuesday,wednesday,thursday,friday,saturday,sunday,pto_used,date_friday)
 VALUES(2, 0,0,8,8,8,8,8, 0, '1992-08-18');
INSERT INTO timecards(employee_id,monday,tuesday,wednesday,thursday,friday,saturday,sunday,pto_used,date_friday)
 VALUES(3, 8,8,8,8,8,0,0, 0, '1992-08-18');
INSERT INTO timecards(employee_id,monday,tuesday,wednesday,thursday,friday,saturday,sunday,pto_used,date_friday)
 VALUES(3, 8,8,8,8,8,0,0, 0, '1992-08-18');
INSERT INTO timecards(employee_id,monday,tuesday,wednesday,thursday,friday,saturday,sunday,pto_used,date_friday)
 VALUES(3, 8,8,8,8,8,0,0, 0, '1992-08-18');
INSERT INTO timecards(employee_id,monday,tuesday,wednesday,thursday,friday,saturday,sunday,pto_used,date_friday)
 VALUES(1, 8,8,8,8,8,0,0, 0, '1992-08-18');
INSERT INTO timecards(employee_id,monday,tuesday,wednesday,thursday,friday,saturday,sunday,pto_used,date_friday)
 VALUES(4, 8,8,8,8,8,0,0, 0, '1992-08-18');
INSERT INTO timecards(employee_id,monday,tuesday,wednesday,thursday,friday,saturday,sunday,pto_used,date_friday)
 VALUES(5, 8,8,8,8,8,0,0, 0, '1992-08-18');
