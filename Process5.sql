USE grocery;
#markup (or down) prices to deal with inflation
DROP PROCEDURE IF EXISTS Average_expendature;

DELIMITER $$
CREATE PROCEDURE  Average_expendature(IN parameter varchar(20))
BEGIN
SELECT AVG(f.total) AS AverageExpendature FROM  (
SELECT SUM(d.item_price) AS total FROM (
    SELECT inventory.item_id, inventory.item_name, inventory.item_price, s.cust_id FROM (
        SELECT sales.trans_id, sales.item_id, a.cust_id FROM (
            SELECT transactions.trans_id, customers.cust_id FROM
              customers,
              transactions
            WHERE
              customers.cust_id = transactions.cust_id
          ) as a,
          sales
        WHERE
          a.trans_id = sales.trans_id
      ) AS s,
      inventory
    WHERE
        inventory.item_id = s.item_id AND
      inventory.item_name LIKE parameter
  ) AS d
    GROUP BY cust_id
) as f;
END $$
DELIMITER  ;

CALL Average_expendature("%bread%");