USE grocery;
DROP TRIGGER IF EXISTS inventory_handler;

#Updating an item in inventory & store balance AFTER transaction
DELIMITER $$

CREATE TRIGGER inventory_handler 
AFTER INSERT ON sales
FOR EACH ROW BEGIN
		UPDATE inventory 
        SET inventory.quantity=inventory.quantity-NEW.quantity 
        WHERE inventory.item_id = NEW.item_id AND inventory.quantity >= NEW.quantity;
END
$$
DELIMITER ;

#Updating customer's points after a purchase
DROP TRIGGER IF EXISTS customer_point_reward ;

DELIMITER $$

CREATE TRIGGER customer_point_reward 
AFTER INSERT ON transactions
FOR EACH ROW BEGIN
		UPDATE customers 
        SET customers.points=customers.points+NEW.price *10
        WHERE customers.cust_id = NEW.cust_id;
END
$$
DELIMITER ;

#Updating employees pto. they get 4 per timecard, but lose pto is any was used
DROP TRIGGER IF EXISTS update_PTO ;

DELIMITER $$

CREATE TRIGGER update_PTO 
AFTER INSERT ON timecards
FOR EACH ROW BEGIN
		UPDATE employees
        SET employees.pto=employees.pto-NEW.pto_used +4
        WHERE employees.employee_id = NEW.employee_id;
END
$$
DELIMITER ;