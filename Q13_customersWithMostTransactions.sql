#Top 10 custoemrs with most transactions
SELECT
  customers.cust_name,
  COUNT(customers.cust_id) AS number_of_transactions
FROM  transactions NATURAL JOIN customers
GROUP BY
  customers.cust_id
ORDER BY
  number_of_transactions DESC
LIMIT 10