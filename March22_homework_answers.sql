/*SELECT employee_name, position FROM employees WHERE position = 'manager';## 1.How many employees are managers
SELECT COUNT(*) FROM customers WHERE (address = 'metro area');## .6 How many customers are from the metro area.
SELECT cust_name from customers WHERE cust_name RLIKE '^[SQLsql]'; ##7. hOW MANY Customers have names ending in s q or l.
SELECT cust_name, points FROM customers WHERE (customers.points>=1000);#nope
SELECT * FROM transactions order by price DESC LIMIT 10;# 10. What are the 10 largest transactions
SELECT employee_name, TIMESTAMPDIFF(YEAR ,hired,current_date()) AS length from employees ORDER BY hired LIMIT 1;# First employee hired
SELECT employee_name, pto FROM employees WHERE pto = (SELECT MAX(PTO)  FROM employees); #Employee with most PTO
SELECT FLOOR(AVG(TIMESTAMPDIFF(YEAR ,hired,current_date()))) as Average_Years_of_Employment_Managers from Employees where position= 'Manager';#Average employee length of managers
SELECT employee_name FROM employees JOIN customers ON employees.employee_name = customers.cust_name; #Which employees are also customers JOIN
SELECT  MIN( points)FROM customers; #What is the smallest balance of points a customer has.
*/
#####################Querys 2.0#######################
#1. number of customers in Sunset district LOOKS FOR WORD SUNSET DISTRICT  anywhere inside address FUNCTIONAL
SELECT COUNT(cust_name) FROM customers WHERE customers.address RLIKE '(Sunset District)+';
#2. Spent on scones		FUNCTIONAL
#SELECT SUM( transactions.quantity*inventory.item_price) FROM transactions INNER JOIN inventory ON transactions.item_id = inventory.item_id WHERE item_name = 'scones';
SELECT SUM( s.quantity*i.item_price)  as Sales_from_scones FROM sales s
	INNER JOIN inventory i ON s.item_id = i.item_id 
    WHERE item_name = 'scones';
#3.Top 5 spending customers	WAITING FOR RYAN TO DECIDE ON A TRANS CUSTOMER TABLE LAYOUT.
SELECT cust_name, SUM( s.quantity*item_price) AS money_spent from customers c 
	inner join transactions t  on c.cust_id = t.cust_id
	inner join sales s on t.trans_id = s.trans_id
	inner join inventory i on s.item_id = i.item_id
group by cust_name order by money_spent desc limit 5;

#4. What category have we recieved the least money from.
SELECT category, quantity*item_price as Total from inventory i
	inner join sales s on i.item_id = s.item_id
    inner join transactions t on s.trans_id = t.trans_id
group by category order by  Total ASC LIMIT 1;

#5.What are the average daily profits over the past week?
SELECT sum(quantity*item_price)/7 as average_daily_profit from inventory i
	inner join sales s on i.item_id = s.item_id
    inner join transactions t on t.trans_id = s.trans_id
where trans_time >= curdate() - INTERVAL DAYOFWEEK(CURDATE())+6 DAY;


