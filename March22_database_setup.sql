drop database grocery;
CREATE database IF NOT EXISTS grocery;
use grocery;

CREATE table IF NOT EXISTS employees(
	employee_id integer PRIMARY KEY NOT NULL AUTO_INCREMENT,
	employee_name varchar(20),
    position varchar(20),
    phone varchar(10),
    hired date,
    fired date,
    sex char,
    pto decimal, 
    address varchar(32)
);
CREATE table IF NOT EXISTS inventory(
	item_id integer PRIMARY KEY NOT NULL AUTO_INCREMENT,
    item_name varchar(20),
    stock_quantity integer,
    item_price decimal(6, 2),
    category varchar(20)
);

CREATE table IF NOT EXISTS customers(
	cust_id integer PRIMARY KEY NOT NULL AUTO_INCREMENT,
    cust_name varchar(20),
    points integer(9),
    phone varchar(10),
    address varchar(40)
);


CREATE table IF NOT EXISTS transactions(
	trans_time DATETIME DEFAULT CURRENT_TIMESTAMP,
    trans_id integer PRIMARY KEY NOT NULL AUTO_INCREMENT,
    employee_id integer NOT NULL,
    trans_type varchar(20),
    cust_id integer,	#CANT BE FOREIGN KEY DOES NOT MEEN WE REMOVE THIS!
    ##FOREIGN KEY (cust_id)  references customers(cust_id),	#Cant be unique or foreign may equal null 
    FOREIGN KEY (employee_id) references employees(employee_id)	#What happens to this table if we fire an employee? 
);

CREATE TABLE IF NOT EXISTS sales(#  What is point? transaction does everything this does and more.
	sales_id integer PRIMARY KEY NOT NULL AUTO_INCREMENT,
    trans_id integer NOT NULL,
    item_id integer NOT NULL,
    quantity integer NOT NULL default 0,
    FOREIGN KEY (trans_id) references transactions(trans_id),
    FOREIGN KEY (item_id) references inventory(item_id)
);


CREATE TABLE IF NOT EXISTS timecards(
	timecard_id integer PRIMARY KEY NOT NULL AUTO_INCREMENT,
    employee_id integer NOT NULL,
    monday decimal(4,2),
    tuesday decimal(4,2),
    wednesday decimal(4,2),
    thursday decimal(4,2),
    friday decimal(4,2),
    saturday decimal(4,2),
    sunday decimal(4,2),
    pto_used decimal(4,2),
    date_friday DATE,
    FOREIGN KEY (employee_id) REFERENCES employees(employee_id)
);

CREATE VIEW cust_trans_sales AS
	(SELECT * FROM customers NATURAL JOIN 
		(SELECT * FROM transactions NATURAL JOIN sales) as trans_sales);
/*
INSERT INTO customers (cust_name, points, phone, address) VALUES('Sol', 1001, NULL, 'Metro Sunset District area');
INSERT INTO customers (cust_name, points, phone, address) VALUES('ManLey', 1000, NULL, 'Sunset District');
INSERT INTO customers (cust_name, points, phone, address) VALUES('Lenny', 12, NULL, 'Mako Rd.');
INSERT INTO customers (cust_name, points, phone, address) VALUES('Bobby', 12, NULL, NULL);

INSERT INTO employees(employee_name, position, phone, hired, fired, sex, pto, address) VALUES( 'Lenny', 'Manager', 8045559999, '1999-10-12', null, 'm', 200, '2600 malchite dr.');
INSERT INTO employees(employee_name, position, phone, hired, fired, sex, pto, address) VALUES( 'Bobby', 'Cashier', 7035559989, '1992-08-18', null, 'm', 0, '2800 dirt dr.');

INSERT INTO inventory( item_name, stock_quantity, item_price, category) VALUES ( 'bread', 99, 3.50, 'baked');
INSERT INTO inventory( item_name, stock_quantity, item_price, category) VALUES ( 'scones', 12, 2.99, 'baked');
INSERT INTO inventory( item_name, stock_quantity, item_price, category) VALUES ( 'canned beans', 1, 3.50, 'canned');
INSERT INTO inventory( item_name, stock_quantity, item_price, category) VALUES ( 'canned bread', 20, 3.50, 'canned');
INSERT INTO inventory( item_name, stock_quantity, item_price, category) VALUES ( 'salt & vinegar chips', 15, 2.50, 'chips');

INSERT INTO transactions( item_id, employee_id, cust_id, quantity, trans_type)  VALUES( 1, 1, 1, 4, 'purchase');#4 bread bought by Sol checked out by Lenny
INSERT INTO transactions( item_id, employee_id, cust_id, quantity, trans_type)  VALUES( 1, 1, 1, 4, 'purchase');#4 bread bought by Sol checked out by Lenny
INSERT INTO transactions( item_id, employee_id, cust_id, quantity, trans_type)  VALUES( 2, 2, 2, 5, 'purchase');#5 scones bought by ManLey, checked by Bobby
INSERT INTO transactions( item_id, employee_id, cust_id, quantity, trans_type)  VALUES( 3, 1, 4, 1, 'purchase');# 1 can of beans bought by Lenny, checked out by Lenny
*/

