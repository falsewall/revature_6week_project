USE grocery;
SELECT  item_name,count(item_name) as occurences FROM  (
	SELECT i.item_id, inventory.item_name FROM (
		SELECT t.trans_id,item_id FROM (
			SELECT trans_id FROM (
				SELECT * FROM
				  customers
				WHERE
				  points > 1000
			) AS m,
			transactions
			WHERE
			  transactions.cust_id = m.cust_id
		) AS t,
		sales
		WHERE
		  t.trans_id = sales.trans_id
		) as i,
	inventory 
    WHERE inventory.item_id = i.item_id
	) AS f
GROUP BY  item_name ORDER BY  occurences ASC LIMIT  1;